<?php 
/**
 * Template Name: Business Template
 */
get_header();

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// For Free Version
if ( ! function_exists( 'wphester_plus_activate' ) ):

	if ( function_exists( 'spiceb_activate' ) ):

		do_action( 'spiceb_wphester_slider_action' , false);
		do_action( 'spiceb_wphester_services_action', false);
		do_action( 'spiceb_wphester_testimonial_action' ,false);
		do_action( 'spiceb_wphester_team_action' ,false);
		do_action( 'spiceb_wphester_news_action' ,false);
	else:
	?>
	<p class="wphester alert alert-warning text-center" role="alert">
	<?php echo esc_html__('This template shows the homepage sections, and to show these sections you have to activate the companion plugin.','wphester' );?>
	</p>
	<?php
	endif;

endif;

// For Pro Version
if ( function_exists( 'wphester_plus_activate' ) ):
		$wphester_front_page = get_theme_mod('front_page_data','services,fun,portfolio,news,cta,about,testimonial,team,wooproduct,callout,client');
		do_action( 'wphester_plus_before_slider_section_hook', false);
		do_action( 'wphester_plus_slider_action' , false);		
		do_action( 'wphester_plus_after_slider_section_hook', false);
	    $wphester_data =is_array($wphester_front_page) ? $wphester_front_page : explode(",",$wphester_front_page);			
		if($wphester_data) 
		{
			foreach($wphester_data as $wphester_key=>$wphester_value)
			{	
                do_action( 'wphester_plus_before_'.$wphester_value.'_section_hook', false);
				
				do_action( 'wphester_plus_'.$wphester_value.'_action', false);
				
				do_action( 'wphester_plus_after_'.$wphester_value.'_section_hook', false);

			}
		}

endif;
get_footer();