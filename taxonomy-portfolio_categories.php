<?php
get_header();
$portfolio_cat_title1 = get_theme_mod('porfolio_category_page_title', __('Portfolio Category Title', 'wphester-plus'));
$portfolio_cat_desc = get_theme_mod('porfolio_category_page_desc', __('Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.', 'wphester-plus'));
$portfolio_cat_column_layout = get_theme_mod('portfolio_cat_column_layout', 4);
$tax = 'portfolio_categories';    
$term_args = array('hide_empty' => true, 'orderby' => 'id');
$tax_terms = get_terms($tax, $term_args);
?>
<section class="section-space portfolio portfolio-cat-page">
    <div class="container<?php echo esc_html(wphester_container());?>">
        <?php if (!empty($portfolio_cat_title1) || !empty($portfolio_cat_desc)): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header text-center">
                        <?php if (!empty($portfolio_cat_title1)) : ?><h2 class="section-title"><?php echo $portfolio_cat_title1; ?></h2>
                        <?php if (!empty($portfolio_cat_desc)) : ?><p class="section-subtitle"><?php echo $portfolio_cat_desc; ?></p><?php endif; ?>
                       <?php endif; ?>
                        
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="tab-content">
                <!--Panel 1-->
                <div class="tab-pane fade show in active" id="panel31" role="tabpanel">
                <?php
                $norecord = 0;
                $args = array(
                    'post_status' => 'publish');
                $portfolio_query = null;
                $portfolio_query = new WP_Query($args);
                if (have_posts()):
                    ?>

                    <div class="row">
                    <?php
                    while (have_posts()) : 
                        the_post();
                        $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                        if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                            $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                        } else {
                            $portfolio_link = '';
                        } 
                        $portfolio_description = get_the_content();
                        ?>
                        <!-- Grid column -->
                        <div class="col-lg-<?php echo $portfolio_cat_column_layout;?> col-md-<?php echo $portfolio_cat_column_layout;?>">
                        <!-- Card -->
                            <div class="card">
                                <figure class="portfolio-thumbnail">
                                    <?php
                                    the_post_thumbnail('full', array('class' => 'img-fluid'));
                                    if (has_post_thumbnail()) {
                                        $post_thumbnail_id = get_post_thumbnail_id();
                                        $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                    }

                                    if (!empty($portfolio_link)) {
                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                        $portlink = '<a href=' . "$portfolio_link" . ' title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                    } else {
                                        $portlink ='<a href="#" title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                    }
                                    foreach ($tax_terms as $tax_term) {
                                        $modelId = get_the_ID() . '_model'.rawurldecode($tax_term->slug);
                                    }?>
                                    <a data-toggle="modal" data-target="#<?php echo $modelId; ?>"><i>+</i></a>
                                </figure>

                                <figcaption>
                                    <?php if(!empty($portlink)):?>
                                        <div class="entry-header">
                                            <h4 class="entry-title">
                                                <?php echo $portlink; ?>
                                            </h4>
                                        </div>
                                    <?php endif;
                                    $tax_string=implode(" ",get_the_taxonomies());
                                    $tax_cat=str_replace( array( 'Categories:'), ' ', $tax_string);?>
                                    <p class="taxonomy-list"><?php echo $tax_cat;?></p>
                                </figcaption>
                            </div>
                            <!-- Card -->                         

                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $modelId;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body p-0">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            
                                            <!-- Grid row -->
                                            <div class="row">
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6 py-5 pl-5">                                                 
                                                    <article class="post text-center">
                                                        <div class="entry-header">
                                                            <h2 class="entry-title"><?php echo $portlink; ?></h2>
                                                        </div>                                                        
                                                        <div class="entry-content">
                                                                <p><?php echo get_the_content(); ?></p>
                                                        </div>                                                        
                                                    </article>                                                    
                                                </div>
                                                <!-- Grid column -->
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6">
                                                    <div class="view rounded-right">
                                                        <img class="img-fluid" src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title();?>">
                                                    </div>
                                                </div>
                                                <!-- Grid column --> 
                                            </div>
                                            <!-- Grid row -->                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal -->
                        </div>
                        <!-- /Grid column -->
                        <?php
                        endwhile;
                        $norecord = 1;
                        wp_reset_query();
                        ?>									
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>		
</section>
<?php get_footer(); ?>