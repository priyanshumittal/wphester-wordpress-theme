<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wphester
 */
get_header();?>
<section class="section-space error-page">
    <div class="container<?php echo esc_html(wphester_container());?>">         
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <h2 class="title"><?php echo '4'; ?><img src="<?php echo esc_url(WPHESTER_TEMPLATE_DIR_URI.'/assets/images/error-image.png');?>" class="img-fluid" alt="<?php esc_attr_e('error-page','wphester'); ?>"><?php echo '4'; ?></h2>
                <p class="contact-subtitle"><?php _e("Looks like your request link broken.<br> Don't be sad. we have more pages to visit.","wphester");?>
                    
                </p>
                <div class="mx-auto">
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="btn-small btn-color" alt="<?php esc_attr_e('back to home','wphester'); ?>"><?php esc_html_e('Back to homepage', 'wphester' ); ?></a>
                </div>
            </div>
        </div>          
    </div>
</section>
<?php get_footer();?>