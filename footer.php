<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wphester
 */
if ( ! function_exists( 'wphester_plus_activate' ) ):
	do_action('wphester_footer_section_hook');
else:
	do_action('wphester_plus_footer_section_hook');
endif;?>
</div>
</div>
<?php wp_footer(); ?>	
</body>
</html>