<?php 
get_header();
		if('page' == get_option('show_on_front')){ get_template_part('index');}
		elseif(is_home()){ get_template_part('index');}
		elseif ( ! function_exists( 'wphester_plus_activate' ) ){ get_template_part('index');}
		elseif (function_exists( 'wphester_plus_activate' ) )
		{
		$wphester_front_page = get_theme_mod('front_page_data','cta1,services,portfolio,cta2,about,team,news,fun,wooproduct,testimonial,client');
		do_action( 'wphester_plus_before_slider_section_hook', false);
		do_action( 'wphester_plus_slider_action' , false);		
		do_action( 'wphester_plus_after_slider_section_hook', false);
	    
	    $wphester_data =is_array($wphester_front_page) ? $wphester_front_page : explode(",",$wphester_front_page);			
		if($wphester_data) 
		{
			foreach($wphester_data as $wphester_key=>$wphester_value)
			{	
                do_action( 'wphester_plus_before_'.$wphester_value.'_section_hook', false);
				
				do_action( 'wphester_plus_'.$wphester_value.'_action', false);
				
				do_action( 'wphester_plus_after_'.$wphester_value.'_section_hook', false);

			}
		}
	}
get_footer();