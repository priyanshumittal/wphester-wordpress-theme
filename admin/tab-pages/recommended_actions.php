<?php 
	$wphester_actions = $this->recommended_actions;
	$wphester_actions_todo = get_option( 'recommended_actions', false );
?>
<div id="recommended_actions" class="wphester-tab-pane panel-close">
	<div class="action-list">
		<?php if($wphester_actions): foreach ($wphester_actions as $key => $wphester_val): ?>
		<div class="action col-md-6" id="<?php echo esc_attr($wphester_val['id']); ?>">
			<div class="action-box">
				<div class="action-watch">
				<?php if(!$wphester_val['is_done']): ?>
					<?php if(!isset($wphester_actions_todo[$wphester_val['id']]) || !$wphester_actions_todo[$wphester_val['id']]): ?>
						<span class="dashicons dashicons-visibility"></span>
					<?php else: ?>
						<span class="dashicons dashicons-hidden"></span>
					<?php endif; ?>
				<?php else: ?>
					<span class="dashicons dashicons-yes"></span>
				<?php endif; ?>
				</div>
				<div class="action-inner">
					<h3 class="action-title"><?php echo esc_html($wphester_val['title']); ?></h3>
					<div class="action-desc"><?php echo esc_html($wphester_val['desc']); ?></div>
					<?php echo wp_kses_post($wphester_val['link']); ?>
				</div>
			</div>
		</div>
		<?php endforeach; endif; ?>
	</div>
</div>