<?php
/**
 * Getting started template
 */
$wphester_name = wp_get_theme();?>
<div id="getting_started" class="wphester-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="wphester-info-title text-center">
					<?php
					/* translators: %s: theme name */
					printf( esc_html__('%s Theme Configuration','wphester'), esc_html($wphester_name) ); ?>
				</h1>
			</div>
		</div>
		<div class="row" style="margin-top: 20px;">

			<div class="col-md-12">
			    <div class="wphester-page" style="border: none;box-shadow: none;">
					<div class="mockup">
			    		<img src="<?php echo esc_url(WPHESTER_TEMPLATE_DIR_URI.'/admin/assets/img/mockup-lite.png');?>"  width="100%">
			    	</div>
				</div>	
			</div>	

		</div>
		
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-6"> 
				<div class="wphester-page">
					<div class="wphester-page-top">
						<?php 
						/* translators: %s: theme name */
						printf( esc_html__( 'Additional features in %s Plus', 'wphester' ), esc_html($wphester_name) ); ?>
					</div>
					<div class="wphester-page-content">
						<ul class="wphester-page-list-flex">
							<li>
								<?php echo esc_html__('Unlimited Slides','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Unlimited Services','wphester'); ?>
							</li>					
							<li>
								<?php echo esc_html__('Header Presets','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Portfolio Section','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Funfact Section','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Client Section','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Multiple Blog Templates','wphester'); ?>
							</li>
							<li>
 								<?php echo esc_html__('Predefined Color Schemes','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Multiple Preloader Designs','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Multiple Search Effects','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Post Navigation Styles','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('WPML Support','wphester'); ?>
							</li>
							
							<li>
								<?php echo esc_html__('Drag and Drop Section Orders','wphester'); ?>
							</li>
							
							<li>
								<?php echo esc_html__('Team Section With Grid Effect','wphester'); ?>
							</li>
							
							<li>
								<?php echo esc_html__('Shop Section With Unlimited Items','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Shop Section With Carousel Effect','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Testimonial Section With Grid Effect','wphester'); ?>
							</li>
							<li>
								<?php echo esc_html__('Homepage Sections Before/After Hooks','wphester'); ?>
							</li>
							
							<li>
								<?php echo esc_html__('Homepage Sections Shortcode','wphester'); ?>
							</li>
							
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-6"> 
				<div class="wphester-page">
					<div class="wphester-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'wphester' ); ?></div>
					<div class="wphester-page-content">
						<ul class="wphester-page-list-flex">
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog Options','wphester'); ?></a>
							</li>
							 <li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Footer Widgets','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage Sections','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General Settings','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=colors_back_settings' ) ); ?>" target="_blank"><?php esc_html_e('Colors & Background','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography Settings','wphester'); ?></a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Theme Style Settings','wphester'); ?></a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="wphester-page">
					<div class="wphester-page-top"><?php esc_html_e( 'Useful Links', 'wphester' ); ?></div>
					<div class="wphester-page-content">
						<ul class="wphester-page-list-flex">
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://wphester.spicethemes.com/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf ( esc_html__('%s Demo','wphester'), esc_html($wphester_name) ); ?>
								</a>
							</li>
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://wphester-pro.spicethemes.com/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf ( esc_html__('%s Plus Demo','wphester'), esc_html($wphester_name) ); ?>								
								</a>
							</li>

							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://wordpress.org/support/theme/wphester/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf ( esc_html__('%s Theme Support','wphester'), esc_html($wphester_name) ); ?>
								</a>
							</li>
														
						    <li> 
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://wordpress.org/support/theme/wphester/reviews/#new-post'); ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','wphester'); ?></a>
							</li>
							
							<li>
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-plus'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf ( esc_html__('%s Plus Details','wphester'), esc_html($wphester_name) ); ?>
								</a>
							</li>
							
						    <li> 
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/contact/'); ?>" target="_blank"><?php echo esc_html__('Pre-sales enquiry','wphester'); ?></a>
							</li> 

							<li> 
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-free-vs-plus/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','wphester'); ?></a>
							</li> 

							<li> 
								<a class="wphester-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','wphester'); ?></a>
							</li> 
						</ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>