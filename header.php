<?php
/**
 * The header for our theme
 *
 * @package wphester
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
        <?php if (is_singular() && pings_open(get_queried_object())) : 
            echo '<link rel="pingback" href=" '.esc_url(get_bloginfo( 'pingback_url' )).' ">';
        endif;
        wp_head(); ?>   
    </head>
<?php
if(get_theme_mod('banner_enable',true)==true) { $wphester_banner='banner'; } else { $wphester_banner='remove-banner'; }
if(get_theme_mod('wphester_layout_style', 'wide') == "boxed") { $wphester_class = "boxed"; } else { $wphester_class = "wide"; } ?>
<body <?php body_class($wphester_class." ". $wphester_banner." ".get_theme_mod('wphester_color_skin','dark')); ?> >
    <?php wp_body_open(); ?>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wphester'  ); ?></a>
               <div id="wrapper"> 
                <?php 
                global $template;
                global $woocommerce;
                        
                if ( ! function_exists( 'wphester_plus_activate' ) ):
                    do_action('wphester_preloader_feaure_section_hook');         
                    get_template_part('inc/header/header-nav');
                    if(basename($template)!='template-business.php'):
                        wphester_breadcrumbs();
                    endif;
                else:   
                    do_action('wphester_plus_preloader_feaure_section_hook');                     
                    do_action('wphester_plus_header_feaure_section_hook');
                    if(basename($template)!='template-business.php'):
                        wphester_breadcrumbs();
                    endif;
                endif;?>
                    <div id="content">