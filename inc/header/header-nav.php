<div class="<?php if(get_theme_mod('sticky_header_enable',false)===true):?>header-sticky<?php endif;?>">
<header class="header-sidebar layout3">
	<div class="bottom-header">
	  	<div class="container">
	  		<div class="row">
	  			<div class="col-lg-4 col-md-4 col-sm-12">
	  				<?php the_custom_logo(); do_action('wphester_plus_sticky_header_logo');
					$wphester_header_text=get_theme_mod('header_text',true);
					if ( $wphester_header_text==true ) :
						if((get_option('blogname')!='') || (get_option('blogdescription')!='')):?>
							<div class="custom-logo-link-url"> 
								<?php if(get_option('blogname')!=''):?>
					    			<h2 class="site-title"><a class="site-title-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
					    			</h2>
					    		<?php endif;
								$description = get_bloginfo( 'description', 'display' );
								if(get_option('blogdescription')!='')
								{
									if ( $description || is_customize_preview() ) : ?>
										<p class="site-description"><?php echo esc_html($description); ?></p>
									<?php endif; 
								}?>
							</div>
						<?php endif;
					endif;?>
	  			</div>
	  			<?php do_action('wphester_menu_header_info');?>  				
	  		</div>
		</div>
	</div>
</header>

<nav class="navbar navbar-expand-lg navbar-dark custom navbar1">
	<div class="container">

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php esc_attr_e('Toggle navigation', 'wphester' ); ?>">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<div class="<?php echo (!is_rtl()) ? "mr-auto" : "mr-auto"; ?>">
			<?php 
			$wphester_shop_button = '<ul class="nav navbar-nav mr-auto">%3$s';
			   //Hence This condition only work when woocommerce plugin will be activate
		    if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { $wphester_target="_blank";}
		 	else { $wphester_target="_self"; }
		 	if((get_theme_mod('after_menu_btn_txt')!='') && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
          		$wphester_shop_button .= '<li class="nav-item  menu-item main-header-button"><a target="'.$wphester_target.'" class="theme-btn btn-style-one main-header-btn" href='.get_theme_mod('after_menu_btn_link','#').'>'.get_theme_mod('after_menu_btn_txt').'</a>';
        	endif;
        	if((get_theme_mod('after_menu_btn_txt')!='') && (get_theme_mod('after_menu_multiple_option')=='html')):
         		 $wphester_shop_button .= '<li class="nav-item radix-btn menu-item radix-html">'.get_theme_mod('after_menu_html');  
        	endif;
			$wphester_shop_button .= '</li>';
			$wphester_shop_button .= '</ul></div></div>'; 
		   
		   //Hence This condition only work when woocommerce plugin will be activate
			if(get_theme_mod('search_btn_enable',false)==true){
		    $wphester_shop_button .= '<div class="options-box clearfix">
			    						<div class="header-module">
							                <div class="nav-search nav-light-search wrap">
						                        <div class="search-box-outer">  
												    <a href="#" class="search-icon  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    	<i class="fa fa-search"></i>
												    	<span class="sub-arrow"></span>
												    </a>
												    <ul class="dropdown-menu search-panel custom-header-preset-panel">
						                            	<li class="panel-outer">
						                             		<div class="form-container">
						                                		<form role="'.esc_attr('Search','wphester').'" method="get" class="search-form" action="'.esc_url( home_url( '/' )).'">
						                                 			<label>
						                                  				<input type="search" class="search-field" placeholder="'.esc_attr__('Search','wphester').'" value="" name="s">
						                                 			</label>
						                                 			<input type="submit" class="search-submit" value="'.__('Search','wphester').'">
						                                		</form>                   
						                               		</div>
						                             	</li>
						                            </ul>						                      
							                    </div>
							                </div>';
		                   			}
			   if ( class_exists( 'WooCommerce' ) ) {
			   		if(get_theme_mod('cart_btn_enable',false)==true ){		   	  
					   $wphester_shop_button .='<div class="cart-header">';
					      global $woocommerce; 
					      $wphester_link = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
					      $wphester_shop_button .= '<a class="cart-icon" href="'.esc_url($wphester_link).'" >';
					      
					      if ($woocommerce->cart->cart_contents_count == 0){
					          $wphester_shop_button .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
					        }
					        else
					        {
					          $wphester_shop_button .= '<i class="fa fa-cart-plus" aria-hidden="true"></i>';
					        }
					           
					        $wphester_shop_button .= '</a>';

					         /* translators: %d: count for number of products in cart */
					        $wphester_shop_button .= '<a class="cart-total" href="'.esc_url($wphester_link).'" ><span>'.sprintf(_n('%d <span>item</span>', '%d <span>items</span>', $woocommerce->cart->cart_contents_count, 'wphester' ), $woocommerce->cart->cart_contents_count).'</span></a>';

					    }
					}
				
			   $wphester_menu_class='';
			    wp_nav_menu( array
			             (
			             'theme_location'=> 'wphester-primary', 
			             'menu_class'    => 'nav navbar-nav mr-auto '.$wphester_menu_class.'',
			             'items_wrap'    => $wphester_shop_button,
			             'fallback_cb'   => 'wphester_fallback_page_menu',
			             'walker'        => new WPHester_Nav_Walker()
			             )); ?>
		</div>
</nav>
</div>