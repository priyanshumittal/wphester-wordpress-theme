<?php
/**
 * Single Blog Options Customizer
 *
 * @package wphester
 */
function wphester_blog_page_options_customizer ( $wp_customize )
{
		// News section title
		$wp_customize->add_setting( 'blog_page_title_option',array(
		'capability'     => 'edit_theme_options',
		'default' => esc_html__('Home','wphester' ),
		'sanitize_callback' => 'wphester_sanitize_text',
		));	
		$wp_customize->add_control( 'blog_page_title_option',array(
		'label'   => esc_html__('Main Title','wphester' ),
		'section' => 'wphester_blog_section',
		'type' => 'text',
		'priority' => 1,
		));

}
add_action( 'customize_register', 'wphester_blog_page_options_customizer' );