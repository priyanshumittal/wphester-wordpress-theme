<?php
/* Notifications in customizer */
 if ( ! function_exists( 'wphester_plus_activate' ) ):
require WPHESTER_TEMPLATE_DIR . '/inc/customizer-notify/wphester-customizer-notify.php';
$wphester_config_customizer = array(
	'recommended_plugins'       => array(
		'spicebox' => array(
			'recommended' => true,
			/* translators: %s: plugin name */
			'description' => sprintf( esc_html__( 'Install and activate the %s plugin to take full advantage of all the features this theme has to offer.', 'wphester'  ), sprintf( '<strong>%s</strong>', 'Spice Box' ) ),
		),
	),
	'recommended_actions'       => array(),
	'recommended_actions_title' => esc_html__( 'Recommended Actions', 'wphester'  ),
	'recommended_plugins_title' => esc_html__( 'Recommended Plugin', 'wphester'  ),
	'install_button_label'      => esc_html__( 'Install and Activate', 'wphester'  ),
	'activate_button_label'     => esc_html__( 'Activate', 'wphester'  ),
	'deactivate_button_label'   => esc_html__( 'Deactivate', 'wphester'  ),
);
WPHester_Customizer_Notify::init( apply_filters( 'wphester_customizer_notify_array', $wphester_config_customizer ) );
endif;