<?php

/**
 * Single Blog Options Customizer
 *
 * @package wphester
 */
function wphester_single_blog_customizer($wp_customize) {
    $wp_customize->add_section('wphester_single_blog_section',
            array(
                'title' => esc_html__('Single Post', 'wphester' ),
                'panel' => 'wphester_theme_panel',
                'priority' => 5
    ));

/*     * *********************** Meta Hide Show ******************************** */
    
    $wp_customize->add_setting('wphester_enable_single_post_admin',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_single_post_admin',
                    array(
                'label' => esc_html__('Hide/Show Author', 'wphester' ),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 4,
                    )
    ));

    $wp_customize->add_setting('wphester_enable_single_post_date',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_single_post_date',
                    array(
                'label' => esc_html__('Hide/Show Date', 'wphester' ),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 5,
                    )
    ));

    $wp_customize->add_setting('wphester_enable_single_post_category',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_single_post_category',
                    array(
                'label' => esc_html__('Hide/Show Category', 'wphester' ),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 6,
                    )
    ));  


    $wp_customize->add_setting('wphester_enable_single_post_comnt',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_single_post_comnt',
                    array(
                'label' => esc_html__('Hide/Show Comments', 'wphester' ),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 8,
                    )
    ));
    $wp_customize->add_setting('wphester_enable_single_post_admin_details',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_single_post_admin_details',
                    array(
                'label' => esc_html__('Hide/Show Author Details', 'wphester' ),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 9,
                    )
    ));
}

add_action('customize_register', 'wphester_single_blog_customizer');