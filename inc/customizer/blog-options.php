<?php
/**
 * Blog Options Customizer
 *
 * @package wphester
 */

function wphester_blog_customizer ( $wp_customize )
{

$wp_customize->add_section('wphester_blog_section', 
	array(
	'title' => esc_html__('Blog Page' , 'wphester' ),
	'panel' => 'wphester_theme_panel',
	'priority' => 4,
));



/******************** Blog Content *******************************/
$wp_customize->add_setting('wphester_blog_content', 
	array(
		'default' 			=> esc_html__('excerpt','wphester' ),
		'sanitize_callback' => 'wphester_sanitize_radio'
		)
	);

$wp_customize->add_control('wphester_blog_content', 
	array(		
		'label' 	=> esc_html__('Choose Options', 'wphester' ),		
		'section' 	=> 'wphester_blog_section',
		'type' 		=> 'radio',
		'priority' => 2,
		'choices' 	=>  array(
			'excerpt' 	=> esc_html__('Excerpt', 'wphester' ),
			'content' 	=> esc_html__('Full Content', 'wphester' ),
			)
		)
	);

/******************** Blog Length *******************************/
$wp_customize->add_setting( 'wphester_blog_content_length',
	array(
		'default'           => 30,
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'wphester_sanitize_number_range',
		)
);
$wp_customize->add_control( 'wphester_blog_content_length',
	array(
		'label'       => esc_html__( 'Excerpt Length', 'wphester'  ),
		'section'     => 'wphester_blog_section',
		'type'        => 'number',
		'priority' => 2,
		'input_attrs' => array( 'min' => 10, 'max' => 200, 'step' => 1, 'style' => 'width: 200px;' ),
	)
);

/************************* Blog Meta Rearrange*********************************/
$default = array( 'blog_author', 'blog_category','blog_comnt');

$choices = array(
        'blog_author' => esc_html__( 'Author', 'wphester'  ),
        'blog_category' => esc_html__( 'Category', 'wphester'  ),
        'blog_comnt' => esc_html__( 'Comment', 'wphester'  ),
    );
    
$wp_customize->add_setting( 'wphester_blog_meta_sort', 
    array(
        'capability'  => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_array',
        'default'     => $default
    ) );

$wp_customize->add_control( new WPHester_Control_Sortable( $wp_customize, 'wphester_blog_meta_sort', 
    array(
        'label' => esc_html__( 'Drag And Drop To Rearrange', 'wphester'  ),
        'section' => 'wphester_blog_section',
        'settings' => 'wphester_blog_meta_sort',
        'type'=> 'sortable',
        'choices'     => $choices
    ) ) );
}
add_action( 'customize_register', 'wphester_blog_customizer' );