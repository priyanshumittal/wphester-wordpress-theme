<?php
/**
 * Helper functions.
 *
 * @package wphester
 */

if (!function_exists('wphester_custom_navigation')) :

    function wphester_custom_navigation() {
        echo '<div class="row justify-content-center center">';
        if (!is_rtl()) {
            the_posts_pagination(array(
                'prev_text' => '<i class="fa fa-angle-double-left"></i>',
                'next_text' => '<i class="fa fa-angle-double-right"></i>',
            ));
        } else {
            the_posts_pagination(array(
                'prev_text' => '<i class="fa fa-angle-double-right"></i>',
                'next_text' => '<i class="fa fa-angle-double-left"></i>',
            ));
        }
        echo '</div>';
    }

endif;
add_action('wphester_post_navigation', 'wphester_custom_navigation');

function wphester_comment($comment, $args, $depth) {
    $tag = 'div';
    $add_below = 'comment';
    ?>
    <div class="media comment-box">
        <span class="pull-left-comment">
    <?php echo get_avatar($comment, 100, null, 'comments user', array('class' => array('img-fluid comment-img'))); ?>
        </span>
        <div class="media-body">
            <div class="comment-detail">
                <h5 class="comment-detail-title">
                    <?php esc_html(comment_author()); ?>
                    <time class="comment-date">
                        <?php 
                         /* translators: %1$s: comment date and %2$s: comment time */
                        printf(('%1$s  %2$s'), esc_html(get_comment_date()), esc_html(get_comment_time())); ?>
                    </time>
                </h5>
                <?php comment_text(); ?>

                <div class="reply">
                <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

if (!function_exists('wphester_posted_content')) :

    /**
     * Content
     *
     */
    function wphester_posted_content() {
        $blog_content = get_theme_mod('wphester_blog_content', 'excerpt');
        $excerpt_length = get_theme_mod('wphester_blog_content_length', 30);

        if ('excerpt' == $blog_content) {
            $excerpt = wphester_the_excerpt(absint($excerpt_length));
            if (!empty($excerpt)) :
                ?>


                <?php
                echo wp_kses_post(wpautop($excerpt));
                ?>


            <?php endif;
        } else {
            ?>

            <?php the_content(); ?>

        <?php }
        ?>
    <?php
    }

endif;



if (!function_exists('wphester_the_excerpt')) :

    /**
     * Generate excerpt.
     *
     */
    function wphester_the_excerpt($length = 0, $post_obj = null) {

        global $post;

        if (is_null($post_obj)) {
            $post_obj = $post;
        }

        $length = absint($length);

        if (0 === $length) {
            return;
        }

        $source_content = $post_obj->post_content;

        if (!empty($post_obj->post_excerpt)) {
            $source_content = $post_obj->post_excerpt;
        }

        $source_content = preg_replace('`\[[^\]]*\]`', '', $source_content);
        $trimmed_content = wp_trim_words($source_content, $length, '&hellip;');
        return $trimmed_content;
    }

endif;

if (!function_exists('wphester_button_title')) :

    /**
     * Display Button on Archive/Blog Page 
     */
    function wphester_button_title() {
        if (get_theme_mod('wphester_enable_blog_read_button', true) == true):
            $blog_button = get_theme_mod('wphester_blog_button_title', 'Read More');
            $btn_icon= is_rtl() ? 'fa fa-long-arrow-left': 'fa fa-long-arrow-right';
            if (empty($blog_button)) {
                return;
            }
            echo '<p><a href = "' . esc_url(get_the_permalink()) . '" class="btn-small more-link">' . esc_html($blog_button) . ' <i class="'.$btn_icon.'"></i></a></p>';

        endif;
    }

endif;

/**
 * Displays the author name
 */
function wphester_get_author_name($post) {

    $user_id = $post->post_author;
    if (empty($user_id)) {
        return;
    }

    $user_info = get_userdata($user_id);
    echo esc_html($user_info->display_name);
}

function wphester_footer_section_hook() {
    ?>
    <footer class="site-footer">  
        <div class="container">
            <?php if (is_active_sidebar('footer-sidebar-1') || is_active_sidebar('footer-sidebar-2') || is_active_sidebar('footer-sidebar-3') | is_active_sidebar('footer-sidebar-4')): ?> 
                <?php get_template_part('sidebar', 'footer');
            endif;?>  
        </div>

        <!-- Animation lines-->
        <div _ngcontent-kga-c2="" class="lines">
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
        </div>
        <!--/ Animation lines-->
        
        <?php if (get_theme_mod('ftr_bar_enable', true) == true): ?>
            <div class="site-info text-center">
            <?php echo wp_kses_post(get_theme_mod('footer_copyright', '<p class="copyright-section"><span>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/wphester-wordpress-theme" rel="nofollow">WPHester</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'wphester').'</span></p>')); ?>     
            </div>
        <?php endif; ?>
         <style type="text/css">
    <?php
    if (get_theme_mod('testimonial_image_overlay', true) != false) {
        $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(0,0,0,0.3)');
        ?>
        .section-space.testimonial:before {
            background-color:<?php echo esc_attr($testimonial_overlay_section_color); ?>;
        }
        
        <?php } ?>

    </style>
    </footer>
    <?php
    $scrolltotop_setting_enable = get_theme_mod('scrolltotop_setting_enable', true);
    if ($scrolltotop_setting_enable == true) {
        ?>
        <div class="scroll-up custom right"><a href="#totop"><i class="fa fa-angle-up"></i></a></div>
    <?php }
}

add_action('wphester_footer_section_hook', 'wphester_footer_section_hook');

if ( ! function_exists( 'wphester_plus_activate' ) ):

//Container Setting For Page
function wphester_container()
{
 
$container_width= "";
return $container_width;
}

//Container Setting For Blog Post
function wphester_blog_post_container()
{

$container_width= "";
return $container_width;
}

//Conainer Setting For Single Post

function wphester_single_post_container()
{
$container_width= "";
return $container_width;
}
//Preloader feature section function
function wphester_preloader_feaure_section_fn(){
if(get_theme_mod('preloader_enable',false)==true):?>
  <div id="preloader1" class="wphester-loader">
        <div class="wphester-preloader-cube">
        <div class="wphester-cube1 wphester-cube"></div>
        <div class="wphester-cube2 wphester-cube"></div>
        <div class="wphester-cube4 wphester-cube"></div>
        <div class="wphester-cube3 wphester-cube"></div>
    </div> </div>
  <?php endif;
}
add_action('wphester_preloader_feaure_section_hook','wphester_preloader_feaure_section_fn');

//Admin customizer preview
if ( ! function_exists( 'wphester_customizer_preview_scripts' ) ) {
    function wphester_customizer_preview_scripts() {
        wp_enqueue_script( 'wphester-customizer-preview', trailingslashit( get_template_directory_uri() ) . 'inc/customizer/customizer-slider/js/customizer-preview.js', array( 'customize-preview', 'jquery' ) );
    }
}
add_action( 'customize_preview_init', 'wphester_customizer_preview_scripts' );

// menu header info
add_action('wphester_menu_header_info','wphester_menu_header_info_fn');
function wphester_menu_header_info_fn(){
    $menu_header_icon1=get_theme_mod('menu_header_icon1');
    $menu_header_title1=get_theme_mod('menu_header_title1');
    $menu_header_text1=get_theme_mod('menu_header_text1');

    $menu_header_icon2=get_theme_mod('menu_header_icon2');
    $menu_header_title2=get_theme_mod('menu_header_title2');
    $menu_header_text2=get_theme_mod('menu_header_text2');

    $menu_header_icon3=get_theme_mod('menu_header_icon3');
    $menu_header_title3=get_theme_mod('menu_header_title3');
    $menu_header_text3=get_theme_mod('menu_header_text3');

    if(!empty($menu_header_icon1) ||!empty($menu_header_title1) || !empty($menu_header_text1)||!empty($menu_header_icon2) ||!empty($menu_header_title2) || !empty($menu_header_text2) || !empty($menu_header_icon3) ||!empty($menu_header_title3) || !empty($menu_header_text3)):?>
        <div class="col-lg-8 col-md-8 col-sm-12">                      
            <div class="head-contact-info">
                <div class="row">
                    <?php  if(!empty($menu_header_icon1) ||!empty($menu_header_title1) || !empty($menu_header_text1)):?>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget first">          
                            <div class="media">
                                <?php if(!empty($menu_header_icon1)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon1);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title1)|| !empty($menu_header_text1)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title1)):?><h4 class="title"><?php echo esc_html($menu_header_title1);?></h4><?php endif;
                                        if(!empty($menu_header_text1)):?><address><?php echo wp_kses_post($menu_header_text1);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div>
                    <?php endif;
                     if(!empty($menu_header_icon2) ||!empty($menu_header_title2) || !empty($menu_header_text2)):?>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget second">
                            <div class="media">
                                <?php if(!empty($menu_header_icon2)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon2);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title2)|| !empty($menu_header_text2)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title2)):?><h4 class="title"><?php echo esc_html($menu_header_title2);?></h4><?php endif;
                                        if(!empty($menu_header_text2)):?><address><?php echo wp_kses_post($menu_header_text2);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div> 
                    <?php endif;
                    if(!empty($menu_header_icon3) ||!empty($menu_header_title3) || !empty($menu_header_text3)):?> 
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget third">
                            <div class="media">
                                <?php if(!empty($menu_header_icon3)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon3);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title3)|| !empty($menu_header_text3)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title3)):?><h4 class="title"><?php echo esc_html($menu_header_title3);?></h4><?php endif;
                                        if(!empty($menu_header_text3)):?><address><?php echo wp_kses_post($menu_header_text3);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div>  
                    <?php endif;?>           
               </div>
            </div>
        </div>
        <?php 
    endif;
}
endif;