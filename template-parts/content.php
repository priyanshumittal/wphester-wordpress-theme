<article  id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>	
	<?php
	if(has_post_thumbnail()):?>
	<figure class="post-thumbnail">
		<?php the_post_thumbnail('full',array('class'=>'img-fluid','alt'=>esc_attr( get_the_title() )));?>			
	</figure>	
	<?php endif;?>	
	<div class="post-content <?php if(!has_post_thumbnail()){ echo 'remove-images';}?>">	

			<div class="entry-date <?php if(!has_post_thumbnail()){ echo 'remove-image';}?>">
	            <a href="<?php echo esc_url(home_url()).'/'.esc_html(date('Y/m', strtotime(get_the_date()))); ?>">
	            <?php echo esc_html(get_the_date()); ?>
	            </a>
	        </div>

		<div class="entry-meta"> 
			<?php 
			$wphester_blog_meta_sort=get_theme_mod( 'wphester_blog_meta_sort', array('blog_author','blog_category','blog_comnt'));  	 
			if ( ! empty( $wphester_blog_meta_sort ) && is_array( $wphester_blog_meta_sort ) ) :
				foreach ( $wphester_blog_meta_sort as $wphester_blog_meta_sort_key => $wphester_blog_meta_sort_val ) :
					if(get_theme_mod('wphester_enable_blog_author',true)==true):
						if ( 'blog_author' === $wphester_blog_meta_sort_val ) :?>
							<i class="fa fa-user"></i>
							<span class="author postauthor">
								<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">
				                <?php echo esc_html(get_the_author()); ?></a>
				            </span>				            
						<?php endif;
					endif;

					if(get_theme_mod('wphester_enable_blog_category',true)==true):
						if ( 'blog_category' === $wphester_blog_meta_sort_val ) :								
                            $wphester_cat_list = get_the_category_list();
                            if (!empty($wphester_cat_list)) {?>
                                <i class="fa fa-folder-open"></i>
                                <span class="cat-links postcat">
                                	<?php the_category(', '); ?>	                                		
                                </span>
                            <?php }
						endif;
					endif;

					if (get_theme_mod('wphester_enable_blog_comnt', true) == true):
						if ( 'blog_comnt' === $wphester_blog_meta_sort_val ) :						
							$wphester_commt = get_comments_number();
		                    if (!empty($wphester_commt)) {
		                        ?>
		                        <i class="fa fa-comment-o"></i>
		                        <span class="cat-links">
		                        	<a href="<?php the_permalink();?>"><?php echo esc_html(get_comments_number());?></a>
		                    	</span>
		                    <?php }
			            endif;
				    endif;
		    	endforeach;
			endif;?> 
		</div>
			
		<header class="entry-header">
			<h4 class="entry-title">
				<a class="rm-h4 blog-title" href="<?php the_permalink();?>"><?php the_title();?></a>
			</h4> 
		</header>
		<div class="entry-content">
		<?php wphester_posted_content();?>
		 <?php
		 $wphester_button_show_hide=get_theme_mod('wphester_blog_content','excerpt');
		 if($wphester_button_show_hide=="excerpt")
		 {
		 if(get_theme_mod('wphester_enable_blog_read_button',true)==true):
		 wphester_button_title();
		 endif;
		} ?>
	</div>
	</div>
</article>